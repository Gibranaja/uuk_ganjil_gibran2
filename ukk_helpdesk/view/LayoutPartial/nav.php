<nav class="side-menu">
	<ul class="side-menu-list">
		
		<li class="grey with-sub">
			<a href="..\Home\">
				<span class="font-icon font-icon-dashboard"></span>
				<span class="lbl">Home</span>
			</a>
		</li>
		
		<li class="blue-dirty">
			<a href="..\TiketKonsultasi\">
				<span class="font-icon font-icon-comments"></span>
				<span class="lbl">Konsultasi</span>
			</a>
		</li>
		
		<li class="green with-sub">
			<a href="..\TiketBaru\">
				<span class="glyphicon glyphicon-duplicate"></span>
				<span class="lbl">Tiket Baru</span>
			</a>
		</li>
		
	</ul>
</nav><!--.side-menu-->