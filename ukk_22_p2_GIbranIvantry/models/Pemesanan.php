<?php
    class Pemesanan extends Connect{
        public function insert_pemesanan($nama_pemesanan, $email, $no_hp, $nama_tamu, $id_tipekamar, $check_in, $check_out, $jml_kamar){

            $connect = parent::connection();
            parent::set_name();

            $sql = "INSERT INTO tb_pemesanan (id_pemesanan, nama_pemesanan, email, no_hp, nama_tamu, id_tipekamar, status_kam, check_in, check_out, jml_kamar, status) VALUES(NULL,?,?,?,?,?,'on site',?,?,?,'1')";

            $sql = $connect->prepare($sql);

            $sql->bindValue(1, $nama_pemesanan);
            $sql->bindValue(2, $email);
            $sql->bindValue(3, $no_hp);
            $sql->bindValue(4, $nama_tamu);
            $sql->bindValue(5, $id_tipekamar);
            $sql->bindValue(6, $check_in);
            $sql->bindValue(7, $check_out);
            $sql->bindValue(8, $jml_kamar);

            $sql->execute();

            return $result=$sql->fetchAll();
        }

        public function get_data(){

            $connect =  parent::connection();
            parent::set_name();

            // sql join table user, ticket, category
            $sql = "SELECT 
                    tb_pemesanan.id_pemesanan,
                    tb_pemesanan.nama_pemesanan, 
                    tb_pemesanan.email, 
                    tb_pemesanan.no_hp, 
                    tb_pemesanan.nama_tamu,
                    tb_pemesanan.id_tipekamar,
                    tb_pemesanan.status_kam,
                    tb_pemesanan.check_in, 
                    tb_pemesanan.check_out, 
                    tb_pemesanan.jml_kamar,
                    tb_tipekamar.tip_name
                    FROM 
                    tb_pemesanan 
                    INNER JOIN tb_tipekamar ON tb_pemesanan.id_tipekamar = tb_tipekamar.id_tipekamar
                    WHERE 
                    tb_pemesanan.status = 1";
            
            $sql = $connect->prepare($sql);
            $sql->execute();
            return $result = $sql->fetchAll();


        }

       
    }