-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 08 Bulan Mei 2022 pada 13.28
-- Versi server: 10.4.21-MariaDB
-- Versi PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_pra`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kamar`
--

CREATE TABLE `tb_kamar` (
  `id_kamar` int(11) NOT NULL,
  `id_tipekamar` int(11) NOT NULL,
  `fasilitas_kamar` text NOT NULL,
  `fasilitas` text NOT NULL,
  `jml_kamar` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_kamar`
--

INSERT INTO `tb_kamar` (`id_kamar`, `id_tipekamar`, `fasilitas_kamar`, `fasilitas`, `jml_kamar`) VALUES
(2, 1, 'full ac, kamar mandi, wifi', 'lobby, customer service, customer care', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pemesanan`
--

CREATE TABLE `tb_pemesanan` (
  `id_pemesanan` int(11) NOT NULL,
  `nama_pemesanan` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `nama_tamu` varchar(30) NOT NULL,
  `id_tipekamar` int(11) NOT NULL,
  `status_kam` varchar(15) NOT NULL,
  `check_in` date NOT NULL,
  `check_out` date NOT NULL,
  `jml_kamar` int(11) NOT NULL,
  `status` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_pemesanan`
--

INSERT INTO `tb_pemesanan` (`id_pemesanan`, `nama_pemesanan`, `email`, `no_hp`, `nama_tamu`, `id_tipekamar`, `status_kam`, `check_in`, `check_out`, `jml_kamar`, `status`) VALUES
(2, 'skcnsk', 'admin@gmail.com', '979797979', 'gibrna', 1, 'on site', '2022-04-26', '2022-04-28', 1, '1'),
(3, 'Aji', 'Aji@gmail.com', '0823131431232', 'Rezki', 2, 'out side', '2022-04-27', '2022-05-06', 1, '1'),
(4, 'Robert', 'test@gmail.com', '08323223', 're', 1, 'on site', '2022-04-28', '2022-04-29', 2, '1'),
(5, 'ewdd', 'dwdw', 'dwdw', 'dww1', 2, 'on site', '2022-04-28', '2022-04-29', 1, '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_tipekamar`
--

CREATE TABLE `tb_tipekamar` (
  `id_tipekamar` int(11) NOT NULL,
  `tip_name` varchar(50) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_tipekamar`
--

INSERT INTO `tb_tipekamar` (`id_tipekamar`, `tip_name`, `status`) VALUES
(1, 'Deluxe', 1),
(2, 'Superior', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `user_nickname` varchar(50) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_pass` varchar(25) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`user_id`, `role_id`, `user_nickname`, `user_email`, `user_pass`, `created_at`, `updated_at`, `deleted_at`, `status`) VALUES
(1, 1, 'Gibran', 'gibran@gmail.com', 'gibrandum69', '2022-04-25 13:36:35', '2022-04-25 13:36:35', '2022-04-25 13:36:35', 1),
(2, 2, 'Vincent', 'vincent@gmail.com', 'gibrandum69', '2022-04-25 13:37:27', '2022-04-25 13:37:27', '2022-04-25 13:37:27', 1);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_kamar`
--
ALTER TABLE `tb_kamar`
  ADD PRIMARY KEY (`id_kamar`);

--
-- Indeks untuk tabel `tb_pemesanan`
--
ALTER TABLE `tb_pemesanan`
  ADD PRIMARY KEY (`id_pemesanan`);

--
-- Indeks untuk tabel `tb_tipekamar`
--
ALTER TABLE `tb_tipekamar`
  ADD PRIMARY KEY (`id_tipekamar`);

--
-- Indeks untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_kamar`
--
ALTER TABLE `tb_kamar`
  MODIFY `id_kamar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `tb_pemesanan`
--
ALTER TABLE `tb_pemesanan`
  MODIFY `id_pemesanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tb_tipekamar`
--
ALTER TABLE `tb_tipekamar`
  MODIFY `id_tipekamar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
