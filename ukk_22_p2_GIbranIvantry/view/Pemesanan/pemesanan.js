function init(){
	$("#ticket-form").on("submit", function(e){
		saveAndEdit(e);
	})
}

		$(document).ready(function() {
			$.post("../../controller/Category.php?op=combo", function(data, status){
				$('#cat_select').html(data); 
			})

		
		});

		function saveAndEdit(e){
			e.preventDefault();
			var formData = new FormData($("#ticket-form")[0]);

			// Validate the form  before insert

			if ($('#check_in').val() == '' || $('#nama_pemesanan').val() == ''){
				swal("Perhatian", "Silahkan Isi Form Terlebih Dahulu", "warning");
			} else {
			$.ajax({
				url: "../../controller/Pemesanan.php?op=insert",
				type: "POST",
				data: formData,
				contentType: false,
				processData: false,
				success: function (data) {
					$('#nama_pemesanan').val('');
					$('#email').val('');
					$('#no_hp').val('');
					$('#nama_tamu').val('');
					$('#check_in').val('');
					$('#check_out').val('');
					$('#jml_kamar').val('');
					swal("Selamat", "Tiket Baru Disimpan", "success");
				}
			})
		}
		}

		init();