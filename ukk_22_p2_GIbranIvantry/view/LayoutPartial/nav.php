<?php

	if($_SESSION["role_id"]==1){
	
?>
	<nav class="side-menu">
		<ul class="side-menu-list">
			
			<li class="grey with-sub">
				<a href="..\Home\">
					<span class="font-icon font-icon-dashboard"></span>
					<span class="lbl">Home</span>
				</a>
			</li>
			
			<li class="blue-dirty">
				<a href="..\DataKamar\">
					<span class="font-icon font-icon-comments"></span>
					<span class="lbl">Data Kamar</span>
				</a>
			</li>
			
			<li class="green with-sub">
				<a href="..\TambahKamar\">
					<span class="glyphicon glyphicon-duplicate"></span>
					<span class="lbl">Tambah Data Kamar</span>
				</a>
			</li>
			
		</ul>
	</nav><!--.side-menu-->


<?php
	}elseif($_SESSION["role_id"]==2){
?>
	<nav class="side-menu">
		<ul class="side-menu-list">
			
			<li class="grey with-sub">
				<a href="..\Home\">
					<span class="font-icon font-icon-dashboard"></span>
					<span class="lbl">Home</span>
				</a>
			</li>
			
			
			<li class="blue-dirty">
				<a href="..\DataPemesan\">
					<span class="font-icon font-icon-comments"></span>
					<span class="lbl">Data Pemesan</span>
				</a>
			</li>
		</ul>
	</nav><!--.side-menu-->
<?php
	}else{
?>

	<nav class="side-menu">
		<ul class="side-menu-list">
			
			<li class="grey with-sub">
				<a href="..\Home\">
					<span class="font-icon font-icon-dashboard"></span>
					<span class="lbl">Home</span>
				</a>
			</li>

			<li class="blue-dirty">
				<a href="..\TiketKonsultasi\">
					<span class="font-icon font-icon-comments"></span>
					<span class="lbl">Konsultasi</span>
				</a>
			</li>
			
			<li class="green">
				<a href="..\KelolaPengguna\">
					<span class="font-icon font-icon-users"></span>
					<span class="lbl">Kelola Pengguna</span>
				</a>
			</li>
		</ul>
	</nav><!--.side-menu-->
<?php
	}
?>