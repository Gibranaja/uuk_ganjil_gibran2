<script src="../../public/js/lib/jquery/jquery.min.js"></script>
	<script src="../../public/js/lib/tether/tether.min.js"></script>
	<script src="../../public/js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="../../public/js/plugins.js"></script>

<script src="../../public/js/app.js"></script>

<!-- summernote editor -->
<script src="../../public/js/lib/summernote/summernote.min.js"></script>

<!-- sweetalert -->
<script type="text/javascript" src="../../public/js/lib/bootstrap-sweetalert/sweetalert.min.js"></script>

<!-- datatable -->
<script src="../../public/js/lib/datatables-net/datatables.min.js"></script>

<!-- date timepicker -->
<script type="text/javascript" src="../../public/js/lib/eonasdan-bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="js/lib/bootstrap-select/bootstrap-select.min.js"></script>
